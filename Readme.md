#### Scraper service

Scrape data of YMCA organisation in NY: https://ymcanyc.org/locations?type&amenities

## Usage
go build && ./scraper <br/>
go run *.go


##### run in docker 
build an image from a Dockerfile <br />
```docker build -t scraper . ``` <br />

run docker container <br />
``` docker run scraper```


