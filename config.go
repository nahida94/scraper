package main

type config struct {
	visitURL        string
	employeeListURL string
	allowedURLs     []string
	workersCount    int
	GoogleAPIKey    string
}

func NewConfig() *config {
	return &config{
		visitURL:        "https://ymcanyc.org/locations?type&amenities",
		employeeListURL: "https://ymcanyc.org/?/about",
		allowedURLs:     []string{"ymcanyc.org"},
		workersCount:    22,
		GoogleAPIKey:    "1RF6rZiGOteylDJo4Y4YLchIii9DzAJS",
	}
}
