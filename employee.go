package main

import (
	"log"
	"strings"

	"github.com/gocolly/colly/v2"
)

var uniqueBranches = make(map[string]struct{})

func collectEmployeeData(collector *colly.Collector, querySelector string, branchChan chan<- *Branch) {
	collector.OnHTML(querySelector, func(e *colly.HTMLElement) {
		//check this branch employees already scraped
		_, ok := uniqueBranches[e.Request.URL.String()]
		if ok {
			return
		}
		uniqueBranches[e.Request.URL.String()] = struct{}{}

		//check branch in context
		branch, ok := e.Request.Ctx.GetAny("branch").(*Branch)
		if !ok {
			log.Println("brach instance does not set to context")
			return
		}

		//start parse employees data
		employees := make([]Employee, 0)
		e.ForEach("p", func(i int, el *colly.HTMLElement) {
			fields := strings.Split(el.Text, "\n")

			//eliminate empty p tags
			if len(fields) > 1 {
				employee := Employee{
					Email: el.ChildText("p > a"),
				}

				switch len(fields) {
				case 3:
					postalAddresses := strings.Split(strings.ToLower(fields[2]), " x")
					employee.Number = postalAddresses[0]
					if len(postalAddresses) > 1 {
						employee.PostalCode = postalAddresses[1]
					}
					fallthrough
				case 2:
					employee.Name = fields[0]
					employee.Title = strings.Replace(fields[1], employee.Email, "", 1)
				}

				employees = append(employees, employee)
			}

		})

		branch.Employees = employees

		branchChan <- branch
	})
}
