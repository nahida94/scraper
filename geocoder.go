package main

import (
	"log"

	"github.com/jasonwinn/geocoder"
)

func initGeoCoder(apiKey string) {
	geocoder.SetAPIKey(apiKey)
}

func getLocation(address string) (float64, float64) {
	lat, long, err := geocoder.Geocode(address)
	if err != nil {
		log.Printf("unable get location from google geocoder: %v \n", err)
		return 0, 0
	}

	return lat, long
}
