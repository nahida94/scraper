module gitlab.com/gemini/scraper

go 1.13

require (
	github.com/gocolly/colly/v2 v2.1.0
	github.com/jasonwinn/geocoder v0.0.0-20190118154513-0a8a678400b8
)
