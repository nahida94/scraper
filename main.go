package main

import (
	"log"
	"net/http"
	"strings"

	"github.com/gocolly/colly/v2"
)

func main() {

	config := NewConfig()
	initGeoCoder(config.GoogleAPIKey)

	branchCollector := colly.NewCollector(
		colly.AllowedDomains(config.allowedURLs...),
	)
	employeeCollector := branchCollector.Clone()

	emptyChan := make(chan struct{}, config.workersCount)
	dataChan := make(chan *Branch, config.workersCount)

	branches := make([]Branch, config.workersCount)

	branchCollector.OnHTML("div.location-list-item", func(e *colly.HTMLElement) {
		url := strings.Replace(config.employeeListURL, "?", e.ChildAttr("span.branch-view-button > a", "href"), 1)
		branch := &Branch{
			Name:        e.ChildText("h2.location-item--title > span"),
			Address:     e.ChildText("div.field-location-direction"),
			PhoneNumber: e.ChildText("div.field-location-phone > a"),
			URL:         url,
		}

		branch.Lat, branch.Long = getLocation(branch.Address)

		//send concurrently request for employees of every branch
		ctx := colly.NewContext()
		ctx.Put("branch", branch)

		go func() {
			emptyChan <- struct{}{}
			employeeCollector.Request(http.MethodGet, url, nil, ctx, nil)
		}()
	})

	// Set error handler
	branchCollector.OnError(func(r *colly.Response, err error) {
		log.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
	})

	//Extract details of the employee
	collectEmployeeData(employeeCollector, "div.left-col > div.field-prgf-2c-left > div.field-sb-body", dataChan)
	collectEmployeeData(employeeCollector, ".field-prgf-description", dataChan)

	// Set error handler
	employeeCollector.OnError(func(r *colly.Response, err error) {
		log.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
	})

	// Start scraping
	err := branchCollector.Visit(config.visitURL)
	if err != nil {
		log.Printf("unable to visit %s\n", config.visitURL)
		return
	}

	for i := 0; i < config.workersCount; i++ {
		<-emptyChan
	}

	for i := 0; i < config.workersCount; i++ {
		branches[i] = *(<-dataChan)
	}
}
