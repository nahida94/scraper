package main

type Employee struct {
	Name       string
	Title      string
	Email      string
	PostalCode string
	Number     string
}

type Branch struct {
	Name        string
	Address     string
	PhoneNumber string
	URL         string
	Lat         float64
	Long        float64
	Employees   []Employee
}
